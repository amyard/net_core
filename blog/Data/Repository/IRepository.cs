﻿using blog.Models;
using blog.Models.Comments;
using blog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Data.Repository
{
    public interface IRepository
    {
        public Post GetPost(int id);
        List<Post> GetAllPosts();
        //IndexViewModel GetAllPosts(int pageNumber);    // for pagination
        IndexViewModel GetAllPosts(int pageNumber, string category);    // for pagination and category
        // List<Post> GetAllPosts(string  category);   // filter by category
        void AddPost(Post post);
        void UpdatePost(Post post);
        void RemovePost(int id);

        void AddSubComment(SubComment comment);

        Task<bool> SaveChangesAsync();
    }
}
