﻿using blog.Models;
using blog.Models.Comments;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Data
{
    public class AppDbContext : IdentityDbContext
    {

        // receive data 
        public AppDbContext(DbContextOptions<AppDbContext> options)
            :base(options)
        {
            
        }


        // create table - each post is a row
        public DbSet<Post> Posts { get; set; }
        public DbSet<MainComment> MainComments { get; set; }
        public DbSet<SubComment> SubComments { get; set; }
    }
}
