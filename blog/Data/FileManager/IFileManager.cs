﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Data.FileManager
{
    public interface IFileManager
    {
        FileStream ImageStream(string image);         // display images
        Task<string> SaveImage(IFormFile image);
        bool RemoveImage(string image);
    }
}
