using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blog.Configuration;
using blog.Data;
using blog.Data.FileManager;
using blog.Data.Repository;
using blog.Services.Email;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace blog
{
    public class Startup
    {


        // using for getting Config from appsettings 
        private IConfiguration _config;
        public Startup(IConfiguration config)
        {
            _config = config;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            // smtp setttings
            services.Configure<SmtpSettings>(_config.GetSection("SmtpSettings"));

            // for sending email
            services.AddSingleton<IEmailService, EmailService>();

            // using our db 
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(_config["DefaultConnection"]));

            // CRUD FOR DB
            services.AddTransient<IRepository, Repository>();

            // save images
            services.AddTransient<IFileManager, FileManager>();

            // for authentication 
            // for admin panel startt using AddIdentity  not  AddDefaultIdentity
            // if left AddDefaultIdentity - we will get error with all logs if use admin panel as not authorized user
            // if AddIdentity - we will get page not found
            //services.AddDefaultIdentity<IdentityUser>(options =>
            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                // for password validations
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
            })
                //.AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>();

            // use redirect method after clicking on admin panel
            services.ConfigureApplicationCookie(options =>
            { 
                options.LoginPath = "/Auth/Login";
            });

            // services.AddMvc(option => option.EnableEndpointRouting = false);
            services.AddMvc(options => {
                options.EnableEndpointRouting = false;

                // cache image
                options.CacheProfiles.Add("Monthly", new CacheProfile { Duration = 60 * 60 * 24 * 7 * 4 });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseMvcWithDefaultRoute();

            /*app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });*/
        }
    }
}
