﻿using blog.Services.Email;
using blog.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Controllers
{
    public class AuthController : Controller
    {

        private SignInManager<IdentityUser> _signInManager;
        private UserManager<IdentityUser> _userManager;
        private IEmailService _emailService;

        public AuthController(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IEmailService emailService
            )
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _emailService = emailService;
        }

        [HttpGet]
        // display login form
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        // catch the login form
        public async Task<IActionResult> Login(LoginViewModel vm)
        {
            var result = await _signInManager.PasswordSignInAsync(vm.UserName, vm.Password, false, false);

            if (!result.Succeeded)
            {
                return View(vm);
            }
            // Todo - REdirect only admin to panel
            var user = await _userManager.FindByNameAsync(vm.UserName);
            var isAdmin = await _userManager.IsInRoleAsync(user, "Admin");

            if(isAdmin)
            {
                return RedirectToAction("Index", "Panel");
            }

            return RedirectToAction("Index", "Home"); 
        }


        [HttpGet]
        // registration
        public IActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        // catch the login form
        public async Task<IActionResult> Register(RegisterViewModel vm)
        {
            if(!ModelState.IsValid)
            {
                return View(vm);
            }

            var user = new IdentityUser
            {
                UserName = vm.Email,
                Email = vm.Email
            };
            var result = await _userManager.CreateAsync(user, "password");

            if(result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);

                // send email
                await _emailService.SendEmail(user.Email, "Welcome", "Thank you for registration");

                return RedirectToAction("Index", "Home");

            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult>  Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
