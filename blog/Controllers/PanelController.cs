﻿using blog.Data.FileManager;
using blog.Data.Repository;
using blog.Models;
using blog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// FOR ADMIN PANEL
namespace blog.Controllers
{
    [Authorize(Roles ="Admin")]        //  only admins have access for panel
    public class PanelController : Controller
    {

        private IRepository _repo;
        private IFileManager _fileManager;
        public PanelController(
            IRepository repo,
            IFileManager fileManager
            )
        {
            _repo = repo;
            _fileManager = fileManager;

        }

        // Index - это имя в папке Views
        public IActionResult Index()
        {
            var posts = _repo.GetAllPosts();
            return View(posts);
        }

        // GET method - after Clicking on Edit Page 
        // int? id  - означает что id = 0 - у нас одна кнопка на Edit and Create 
        // int? id  - дает возможность создавать посты без наличия id
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                // to avoid some error - type new Post() 
                return View(new PostViewModel());
            }
            else
            {
                var post = _repo.GetPost((int)id);
                return View(new PostViewModel
                { 
                    Id = post.Id,
                    Title = post.Title,
                    Body = post.Body,
                    CurrentImage = post.Image,
                    Description = post.Description,
                    Category = post.Category,
                    Tags = post.Tags
                });
            }

        }

        // POST method
        [HttpPost]
        public async Task<IActionResult> Edit(PostViewModel vm)
        {

            var post = new Post
            {
                Id = vm.Id,
                Title = vm.Title,
                Body = vm.Body,
                Description = vm.Description,
                Category = vm.Category,
                Tags = vm.Tags,
            };

            if (vm.Image == null)
                post.Image = vm.CurrentImage;
            else
            {
                // delete old image
                if (!string.IsNullOrEmpty(vm.CurrentImage))
                    _fileManager.RemoveImage(vm.CurrentImage);
                post.Image = await _fileManager.SaveImage(vm.Image);    // use await because it is a task
            }


            if (post.Id > 0)
                _repo.UpdatePost(post);
            else
                _repo.AddPost(post);

            if (await _repo.SaveChangesAsync())
                return RedirectToAction("Index");
            else
                return View(post);
        }


        public async Task<IActionResult> Remove(int id)
        {
            _repo.RemovePost(id);
            await _repo.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
