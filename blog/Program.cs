using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blog.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace blog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // override on two actions
            // first is buildong out webservice
            // second is running our app
            var host = CreateHostBuilder(args).Build();


            try
            {
                // get middleware services from Startup  - public void ConfigureServices(IServiceCollection services)
                var scope = host.Services.CreateScope();
                var ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                var userMng = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
                var roleMng = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                // save the database
                ctx.Database.EnsureCreated();


                var adminRole = new IdentityRole("Admin");
                if (!ctx.Roles.Any())
                {
                    // create role
                    // our Main method is not async so we can use await
                    // we use .GetAwaiter().GetResult()
                    roleMng.CreateAsync(adminRole).GetAwaiter().GetResult();
                }

                if (!ctx.Users.Any(u => u.UserName == "admin"))
                {
                    // create an admin if not exists
                    var adminUser = new IdentityUser
                    {
                        UserName = "admin",
                        Email = "admin@admin.com"
                    };
                    var result = userMng.CreateAsync(adminUser, "password").GetAwaiter().GetResult();

                    // add ro;e to user
                    userMng.AddToRoleAsync(adminUser, adminRole.Name).GetAwaiter().GetResult();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
