﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Helpers
{
    public static class PageHelper
    {
        public static IEnumerable<int> PageNumbers(int pageNumber, int pageCount)
        {

            // fix paggination if we have less than 3 pages
            if (pageCount <= 5)
            {
                for (int i = 1; i <= pageCount; i++)
                {
                    yield return i;
                }
            }
            else
            {
                int midPoint = pageNumber < 3 ? 3
                : pageNumber > pageCount - 2 ? pageCount - 2
                    : pageNumber;

                /*List<int> pages = new List<int>();*/
                /*for (int i = midPoint - 2; i <= midPoint + 2; i++)
                {
                    pages.Add(i);
                }

                // display first page
                if (pages[0] != 1)
                {
                    pages.Insert(0, 1);
                    if (pages[1] - pages[0] > 1)
                    {
                        pages.Insert(1, -1);
                    }
                }

                // last number
                if (pages[pages.Count - 1] != pageCount)
                {
                    pages.Insert(pages.Count, pageCount);
                    if (pages[pages.Count - 1] - pages[pages.Count - 2] > 1)
                    {
                        pages.Insert(pages.Count - 1, -1);
                    }
                }*/

                // the same as above
                int lowerBound = midPoint - 2;
                int upperBound = midPoint + 2;

                // display first page
                if (lowerBound != 1)
                {
                    yield return 1;
                    if (lowerBound - 1 > 1)
                    {
                        yield return -1;
                    }
                }

                for (int i = midPoint - 2; i <= upperBound; i++)
                {
                    yield return i;
                }

                // last number
                if (upperBound != pageCount)
                {
                    if (pageCount - upperBound > 1)
                    {
                        yield return -1;
                    }
                    yield return pageCount;
                }
            }
        }
    }
}
