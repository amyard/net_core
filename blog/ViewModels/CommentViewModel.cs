﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.ViewModels
{
    public class CommentViewModel
    {
        [Required]
        public int PostId { set; get; }
        [Required]
        public int MainCommentId { set; get; }
        [Required]
        public string Message { get; set; }
    }
}
