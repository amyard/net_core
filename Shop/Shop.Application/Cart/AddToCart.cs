﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Newtonsoft.Json;
using Shop.Domain.Models;

namespace Shop.Application.Cart
{
    public class AddToCart
    {
        private ISession _session;

        public AddToCart(ISession session)
        {
            _session = session;
        }


        public class Request
        {
            public int StockId { get; set; }
            public int Qty { get; set; }
        }
        public void Do(Request request)
        {
            var cartProduct = new CartProduct
            {
                StockId = request.StockId,
                Qty = request.Qty
            };
            var stringObject = JsonConvert.SerializeObject(cartProduct);
            _session.SetString("cart", stringObject);
        }
    }
}
