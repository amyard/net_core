﻿using Microsoft.EntityFrameworkCore;
using Shop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Application.StockAdmin
{
    public class GetStock
    {
        private ApplicationDbContext _ctx;

        public GetStock(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }


        public IEnumerable<ProductViewModel> Do()
        {
            /*var stock = _ctx.Stock
                .Where(x => x.ProductId == produtId)
                .Select(x => new StockViewModel 
                { 
                    Id = x.Id,
                    Description = x.Description,
                    Qty = x.Qty
                })                                   // convert our list to StockViewModel  -  need use before ToList
                .ToList();*/

            var stock = _ctx.Products
                .Include(x => x.Stock)     // if we use one model inside another
                .Select(x => new ProductViewModel
                { 
                    Id = x.Id,
                    Description = x.Description,
                    Stock = x.Stock.Select(y => new StockViewModel
                        {
                            Id = y.Id,
                            Description = y.Description,
                            Qty = y.Qty
                    })
                })                            
                .ToList();
            return stock;
        }

        
        public class StockViewModel
        {
            public int Id { get; set; }
            public string Description { get; set; }
            public int Qty { get; set; }
        }

        public class ProductViewModel
        {
            public int Id { get; set; }
            public string Description { get; set; }
            public IEnumerable<StockViewModel> Stock { get; set; }
        }
    }
}
