﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Shop.Application.GetProducts;
using Shop.Database;

namespace Shop.UI.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private ApplicationDbContext _ctx;

        
        // need to fix an error - does not understadn CreateProduct() after loading from folder. there are any dbcontext connection between views and data
        public IndexModel(ILogger<IndexModel> logger,
            ApplicationDbContext ctx
            )
        {
            _logger = logger;
            _ctx = ctx;
        }


        [BindProperty]    // its say it is a main model
        public IEnumerable<GetProducts.ProductViewModel> Products { get; set; }  // get list of products

        public void OnGet()
        {
            Products = new GetProducts(_ctx).Do();
        }
    }
}
